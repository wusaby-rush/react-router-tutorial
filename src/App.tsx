import { RouterProvider, createBrowserRouter } from "react-router-dom";
import RootLayout, {
  loader as rootLoader,
  action as rootAction,
} from "./layouts/RootLayout";
import ErrorPage from "./pages/ErrorPage";
import Contact from "./pages/Contact";
import EditContact from "./pages/EditContact";
import Index from "./pages/index";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage.Component />,
    loader: rootLoader,
    action: rootAction,
    children: [
      {
        index: true,
        ...Index,
      },
      {
        path: "contacts/:id",
        // errors bubble up to the nearest errorElement
        errorElement: <div>Oops! There was an error.</div>,
        // Component: Contact.Component,
        ...Contact,
      },
      {
        path: "contacts/:id/edit",
        loader: Contact.loader,
        ...EditContact,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
